package jp.alhinc.tanaka_ryota.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map.Entry;

public class CalculateSales {
	public static void main(String[] args) throws IOException {
		HashMap<String, String> branchmap = new HashMap<String, String>();
		HashMap<String, Long> branchsummap = new HashMap<String, Long>();
		if (!(args.length == 1)) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		if(!reader("支店定義ファイル","^[0-9]{3}$",args[0],"branch.lst",branchmap,branchsummap)){
			return;
		}
		// 1番（支店定義ファイルの読み込み）の処理

		HashMap<String, String> commoditymap = new HashMap<String, String>();
		HashMap<String, Long> commoditysummap = new HashMap<String, Long>();
		if(!reader("商品定義ファイル","^[0-9a-zA-Z]{8}$", args[0],"commodity.lst",commoditymap,commoditysummap)){
			return;
		}

		// 商品定義ファイルの読み込み

		ArrayList<File> upliftlist = new ArrayList<File>();
		BufferedReader upliftbr = null;
		String data;
		File file = new File(args[0]);
		File[] files = file.listFiles();
		ArrayList<Integer> errorlist = new ArrayList<Integer>();
			for (int i = 0; i < files.length; i++) {
			if (files[i].isFile() && files[i].getName().toString().matches("^\\d{8}.rcd$")) {
					upliftlist.add(files[i]);
					errorlist.add(Integer.valueOf(files[i].getName().substring(0, 8)));
			}
		}

		int error;
		for (error = 0; error < errorlist.size() - 1; error++) {
			if ((errorlist.get(error + 1) - errorlist.get(error)) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
	}


		for (int i = 0; i < upliftlist.size(); i++) {
			ArrayList<String> rcdlist = new ArrayList<String>();
			try {
				FileReader upliftfr = new FileReader(upliftlist.get(i));
				upliftbr = new BufferedReader(upliftfr);
				while ((data = upliftbr.readLine()) != null) {
					rcdlist.add(data);
				}
				if (rcdlist.size() != 3) {
					System.out.println(files[i].getName() + "のフォーマットが不正です");
					return;
				}
				if (!branchsummap.containsKey(rcdlist.get(0))) {
					System.out.println(files[i].getName() + "の支店コードが不正です");
					return;
				}
				if (!commoditysummap.containsKey(rcdlist.get(1))) {
					System.out.println(files[i].getName() + "の商品コードが不正です");
					return;
				}
				if (!(rcdlist.get(2).matches("^[0-9]+$"))) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				Long branchsum = branchsummap.get(rcdlist.get(0));
				branchsum += Long.valueOf(rcdlist.get(2));
				branchsummap.put(rcdlist.get(0), branchsum);
				if (branchsum.toString().length()>10) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
//				支店別集計

				Long commoditysum = commoditysummap.get(rcdlist.get(1));
				commoditysum += Long.valueOf(rcdlist.get(2));
				commoditysummap.put(rcdlist.get(1), commoditysum);
				if (commoditysum.toString().length()>10) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

//				商品別集計


			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if(upliftbr != null) {
					upliftbr.close();
				}
			}
		}

		ArrayList<Entry<String,Long>> branchsort = new ArrayList<Entry<String, Long>>(branchsummap.entrySet());
		Collections.sort(branchsort, new Comparator<HashMap.Entry<String, Long>>() {

			@Override
			public int compare(Entry<String,Long> entry1, Entry<String , Long> entry2) {
				return ((Long)entry2.getValue()).compareTo((Long)entry1.getValue());
			}
		});

		if(!writer(args[0],"branch.out",branchmap,branchsort)) {
			return;
		}



		ArrayList<Entry<String,Long>> commoditysort = new ArrayList<Entry<String, Long>>(commoditysummap.entrySet());
		Collections.sort(commoditysort, new Comparator<HashMap.Entry<String, Long>>() {

			@Override
			public int compare(Entry<String,Long> entry1, Entry<String , Long> entry2) {
				return ((Long)entry2.getValue()).compareTo((Long)entry1.getValue());
			}
		});

		if(!writer(args[0],"commodity.out",commoditymap,commoditysort)) {
			return;
		}
}

	public static boolean writer( String pass, String file,HashMap<String, String> definition,ArrayList<Entry<String,Long>> sort){
		BufferedWriter sumbr = null;
		try {
			File sumfile = new File(pass,file);
			sumfile.createNewFile();
			FileWriter sumfr = new FileWriter(sumfile);
			sumbr = new BufferedWriter(sumfr);
			for (Entry<String, Long> commodity : sort) {
				sumbr.write(commodity.getKey() + "," + definition.get(commodity.getKey()) + "," + commodity.getValue());
				sumbr.newLine();
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(sumbr != null){
				try {
					sumbr.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
				}

			}

		}
		return true;

	}

	public static boolean reader(String name,String regular,String path, String file,HashMap<String, String> definition,HashMap<String, Long> sum){
		BufferedReader br = null;
		try {
			String data;
			File newfile = new File(path, file);
			if (!newfile.exists()) {
				System.out.println(name + "が存在しません");
				return false;
			}
			FileReader fr = new FileReader(newfile);
			br = new BufferedReader(fr);
			while ((data = br.readLine()) != null) {
				String[] items = data.split(",");
				if (!(items.length == 2)) {
					System.out.println(name + "のフォーマットが不正です");
					return false;
				}
				if (!items[0].matches(regular)) {
					System.out.println(name + "のフォーマットが不正です");
					return false;
				}
				definition.put(items[0], items[1]);
				sum.put(items[0], 0L);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました.....");
				}
			}

		}
		return true;
	}


}

